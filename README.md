# fedora-bootstrap
Custom Bootstrap CSS theme for Fedora Apps.

## Setting up ##

Clone the repo:
```
git clone https://pagure.io/fedora-bootstrap.git
```

Install NPM:
```
sudo dnf install npm
```

Install deps for building in NPM
```
npm i
```

## Editing & Trying out the theme ##
### Editing
All changes to the theme are done via files in the `src/` directory. When building the CSS, the upstream bootstrap is pulled down as a dependency and then combined with the overrides and additions in the `src/` dir.
### Try it out
Follow the setting up instuctions above, then run
```
npm run watch
```
This will create a webserver at http://localhost:3000/ for you to see the test implementation with the current theme displayed. Any changes to the source will rebuild the Sass into new CSS and will change in the browser.

## Building Dist files for use
To build the final CSS files, run:

```
npm run build
```

and the created dist directory will contain the JS / CSS files (both minified and not). Note, that previously, the javascript was also rebuilt manually here, but now we just use the stock bootstrap.bundle.js for the javascript. The build script simply copies this to the dist directory.
